function layThongTinTuForm() {
  // lay thong tin tu form
  const _taiKhoan = document.getElementById("tknv").value;
  const _hoVaTen = document.getElementById("name").value;
  const _email = document.getElementById("email").value;
  const _matKhau = document.getElementById("password").value;
  const _ngayLam = document.getElementById("datepicker").value;

  const _luong = document.getElementById("luongCB").value;
  const _chucVu = document.getElementById("chucvu").value;
  const _gioLam = document.getElementById("gioLam").value;
  // tạo object user
  var user = {
    taiKhoan: _taiKhoan,
    hoVaTen: _hoVaTen,
    email: _email,
    matKhau: _matKhau,
    ngayLam: _ngayLam,
    luong: _luong,
    chucVu: _chucVu,
    gioLam: _gioLam,
  };

  return user;
}

function renderUSER(userArr) {
  // render danh sach ra table
  var contentHTML = "";
  for (var index = 0; index < userArr.length; index++) {
    var item = userArr[index];
    var contentTr = `<tr>
                            <td>${item.taiKhoan}</td>
                            <td>${item.hoVaTen}</td>
                            <td>${item.email}</td>
                            <td>${item.ngayLam}</td>
                            <td>${item.chucVu}</td>
                            
                           
                            <td> 
                          <button onclick="xoaNguoiDung('${item.taiKhoan}')" class="btn btn-danger" > Xóa</button>
                            </td>
                            <button onclick="suaNguoiDung('${item.taiKhoan}')" class="btn btn-warning"  data-toggle="modal"
                            data-target="#myModal" > Sửa</button>
                            </td>
                            
                         </tr>`;
    contentHTML += contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, arr) {
  // tìm vị trí
  var viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    var user = arr[index];
    if (user.taiKhoan == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}

function showThongTinLenForm(user) {
  document.getElementById("tknv").value = user.taiKhoan;
  document.getElementById("name").value = user.hoVaTen;

  document.getElementById("email").value = user.email;

  document.getElementById("password").value = user.matKhau;
  document.getElementById("datepicker").value = user.ngayLam;
  document.getElementById("luongCB").value = user.luong;
  document.getElementById("chucvu").value = user.chucVu;
  document.getElementById("gioLam").value = user.gioLam;
}
