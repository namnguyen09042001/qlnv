var listUser = [];
var USER = [];

// lấy dữ liệu từ localStorage
//  object lấy từ localStorage sẽ bị mất các key chứ function ( method )
var listUserJson = localStorage.getItem(USER);
if (listUserJson != null) {
  listUser = JSON.parse(listUserJson);
}
renderUSER(listUser);

function themNguoiDung() {
  var user = layThongTinTuForm();

  listUser.push(user);
  // lưu vào localStorage ( không bị mất dữ liệu khi load trang )

  // convert array listUser thành json
  var listUserJson = JSON.stringify(listUser);
  // lưu data json vào localStorage
  localStorage.setItem(USER, listUserJson);
  renderUSER(listUser);
}

function xoaNguoiDung(idUser) {
  var viTri = timKiemViTri(idUser, listUser);
  if (viTri != -1) {
    listUser.splice(viTri, 1);
    renderUSER(listUser);
  }
}
function suaNguoiDung(idUser) {
  var viTri = timKiemViTri(idUser, listUser);
  if (viTri == -1) {
    return;
  }

  var user = listUser[viTri];
  //   show thông tin lên form
  showThongTinLenForm(user);
}
function capNhatNguoiDung() {
  var user = layThongTinTuForm();

  var viTri = timKiemViTri(user.taiKhoan, listUser);
  if (viTri != -1) {
    listUser[viTri] = user;
    renderUSER(listUser);
  }
  var listUserJson = JSON.stringify(listUser);
  // lưu data json vào localStorage
  localStorage.setItem(USER, listUserJson);
  renderUSER(listUser);
}
